//
//  ISValidationTests.swift
//  ISValidationTests
//
//  Created by Dielson Sales on 28/05/16.
//  Copyright © 2016 Ilhasoft. All rights reserved.
//

import XCTest
import SwiftValidator
@testable import ISValidation

class ISValidationTests: XCTestCase, ValidationDelegate {

    var testSuccessful = false
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let cpfTextField = UITextField()
        let validator = Validator()
        validator.registerField(cpfTextField, rules: [CPFRule()])

        cpfTextField.text = "000.000.000-00"
        validator.validate(self)
        XCTAssert(testSuccessful)

        cpfTextField.text = "00000000000"
        validator.validate(self)
        XCTAssert(testSuccessful)

        cpfTextField.text = "000000000000"
        validator.validate(self)
        XCTAssert(!testSuccessful)

        cpfTextField.text = "000000000-00"
        validator.validate(self)
        XCTAssert(testSuccessful)

        validator.unregisterField(cpfTextField)

        let cepTextField = UITextField()
        let cepValidator = Validator()
        cepValidator.registerField(cepTextField, rules: [CEPRule()])

        cepTextField.text = "00000000"
        cepValidator.validate(self)
        XCTAssert(testSuccessful)

        cepTextField.text = "00.000-000"
        cepValidator.validate(self)
        XCTAssert(testSuccessful)

        cepTextField.text = "00000"
        cepValidator.validate(self)
        XCTAssert(!testSuccessful)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
}

extension ISValidationTests {
    func validationSuccessful() {
        print("Validation successful")
        testSuccessful = true
    }

    func validationFailed(errors: [UITextField:ValidationError]) {
        print("Error")
        testSuccessful = false
    }
}