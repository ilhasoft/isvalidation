//
//  CPFRule.swift
//  ISValidation
//
//  Created by Dielson Sales on 28/05/16.
//  Copyright © 2016 Ilhasoft. All rights reserved.
//

import UIKit
import SwiftValidator

class CPFRule: RegexRule {
    static let regex = "([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})"

    convenience init(message: String = "Não é um CPF válido") {
        self.init(regex: CPFRule.regex, message: message)
    }
}
