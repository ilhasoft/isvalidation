//
//  CEPRule.swift
//  ISValidation
//
//  Created by Dielson Sales on 29/05/16.
//  Copyright © 2016 Ilhasoft. All rights reserved.
//

import UIKit
import SwiftValidator

class CEPRule: RegexRule {
    static let regex = "^[0-9]{2}[\\.]?[0-9]{3}\\-?[0-9]{3}$"

    convenience init(message: String = "Não é um CEP válido") {
        self.init(regex: CEPRule.regex, message: message)
    }
}
